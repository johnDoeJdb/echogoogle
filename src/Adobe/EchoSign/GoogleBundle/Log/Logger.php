<?php

namespace Adobe\EchoSign\GoogleBundle\Log;

class Logger
{
    public static function log($type, $message)
    {
        $now = new \DateTime();
        file_put_contents('/tmp/log_'.$type, $message.' '.$now->format('H:m:s')."\n", FILE_APPEND);
    }

    public static function logObj($type, $variable, $message = '')
    {
        $now = new \DateTime();
        if (is_object($variable) || is_array($variable)) {
            ob_start();
            var_dump($variable);
            $variable = ob_get_clean();
        }
        file_put_contents('/tmp/log_'.$type, $message.' '.$variable.' '.$now->format('H:m:s')."\n", FILE_APPEND);
    }
}
