<?php

namespace Adobe\EchoSign\GoogleBundle\Security;

use Adobe\EchoSign\GoogleBundle\Api\EchoSignApi;
use Adobe\EchoSign\GoogleBundle\Api\GoogleDriveApi;
use Adobe\EchoSign\GoogleBundle\Entity\EchoSignUser;
use Adobe\EchoSign\GoogleBundle\Manager\CryptManager;
use Adobe\EchoSign\GoogleBundle\Manager\DocumentManager;
use Adobe\EchoSign\GoogleBundle\Manager\GoogleRequestManager;
use Adobe\EchoSign\GoogleBundle\Manager\RestoreManager;
use Adobe\EchoSign\GoogleBundle\Manager\UserManager;
use Adobe\EchoSign\GoogleBundle\Model\DriveRequest;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class GoogleAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface, AuthenticationSuccessHandlerInterface
{
    protected $userProvider;
    protected $session;
    protected $userManager;
    protected $googleDriveApi;
    protected $echoSignApi;
    protected $requestStack;
    protected $cryptManager;
    protected $authenticationType;
    protected $queueManager;
    protected $restoreManager;
    protected $authentication;
    protected $requestManager;
    protected $documentManager;

    public function __construct(
        GoogleUserProvider $userProvider,
        Session $session,
        UserManager $userManager,
        GoogleDriveApi $googleDriveApi,
        EchoSignApi $echoSignApi,
        RequestStack $requestStack,
        CryptManager $cryptManager,
        RestoreManager $restoreManager,
        GoogleAuthentication $authentication,
        GoogleRequestManager $requestManager,
        DocumentManager $documentManager
    )
    {
        $this->userProvider = $userProvider;
        $this->session = $session;
        $this->userManager= $userManager;
        $this->googleDriveApi = $googleDriveApi;
        $this->echoSignApi = $echoSignApi;
        $this->requestStack = $requestStack;
        $this->cryptManager = $cryptManager;
        $this->restoreManager = $restoreManager;
        $this->authentication = $authentication;
        $this->requestManager = $requestManager;
        $this->documentManager = $documentManager;
    }

    public function createToken(Request $request, $providerKey)
    {
        $requestParameters = $request->query;
        if ($requestParameters->count()) {
            $this->authentication->authenticate($request);
        }

        $googleRequest = $this->requestManager->fetchRequest();
        if (!$googleRequest || !$googleRequest instanceof DriveRequest || !$googleRequest->getUserId() || !$googleRequest->getFileId()) {
            return new Response("Missed require parameters from request", 403);
        }

        return new PreAuthenticatedToken(
            'anon.',
            "",
            $providerKey
        );
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $googleRequest = $this->requestManager->fetchRequest();
        $this->googleAuthentication();
        $this->echoSignAuthentication();

        $user = $this->userProvider->loadUserByUsername($googleRequest->getUserId());

        return new PreAuthenticatedToken(
            $user,
            "",
            $providerKey,
            $user->getRoles()
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $googleRequest = $this->requestManager->fetchRequest();
        $encryptedState = $this->cryptManager->encrypt($googleRequest);

        if ($this->authenticationType === "GoogleDrive") {
            return new Response("Google Drive token invalid or expired. Please restart application");
        } else {
            return new RedirectResponse($this->echoSignApi->getAuthorizationOAuthUrl($encryptedState, $request->server->get('HTTP_REFERER')));
        }
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $this->restoreManager->restoreFoldersIfTrashed();

        $this->documentManager->downloadQueuedDocuments($this->userManager->fetchCurrentUser());
    }


    private function googleAuthentication()
    {
        $this->authenticationType = "GoogleDrive";
        if (!$this->googleDriveApi->isValidToken()) {
            throw new AuthenticationException(
                'GoogleDrive token invalid'
            );
        }
    }

    private function echoSignAuthentication()
    {
        $this->authenticationType = "EchoSign";
        $echoSignUser = $this->userManager->fetchCurrentEchoSignUser();

        if (!$echoSignUser) {
            throw new AuthenticationException(
                'EchoSign user not found in database'
            );
        }

        if (!$echoSignUser instanceof EchoSignUser) {
            throw new AuthenticationException(
                'Invalid EchoSign user object'
            );
        }

        if (!$echoSignUser->getToken() || !$echoSignUser->isValidToken()) {
            if ($echoSignUser->getRefreshToken()) {
                $refreshToken = $this->userManager->fetchEchoSignRefreshToken();
                $refreshedToken = $this->echoSignApi->refreshOAuthToken($refreshToken);
                if (property_exists($refreshedToken, 'access_token') && property_exists($refreshedToken, 'expires_in')) {
                    $accessToken = $refreshedToken->access_token;
                    $expirationIn = sprintf('+%d seconds', $refreshedToken->expires_in);
                    $expiration = (new \DateTime())->modify($expirationIn);
                    $this->userManager->updateEchoSignToken($accessToken, $expiration);

                    return;
                }
            }
            throw new AuthenticationException(
                'Invalid or expired EchoSign token'
            );
        }
    }
}