<?php

namespace Adobe\EchoSign\GoogleBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\Container;

class GoogleAuthentication
{
    private $container;

    function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function authenticate(Request $request)
    {
        $requestManager = $this->container->get('adobe_echo_sign_google.request_manager');
        $authenticationCode = $requestManager->getAuthCode($request);
        $requestManager->buildDriveRequest($request);

        $this->oAuth2Action($authenticationCode);
        $this->addUserToDatabase();

        return true;
    }

    private function oAuth2Action($code)
    {
        if ($code) {
            $googleDriveApi = $this->container->get('adobe_echo_sign_google.drive_api');
            $router = $this->container->get('router');
            $redirectUrl = $router->generate('google_oauth2callback', array(), true);
            $token = $googleDriveApi->authentication($code, $redirectUrl);
            $this->container->get('session')->set('google_drive_token', $token);
        }
    }

    private function addUserToDatabase()
    {
        $token = $this->container->get('session')->get('google_drive_token');
        $googleRequest = $this->container->get('session')->get('google');
        $userId = $googleRequest->getUserId();
        $userManager = $this->container->get('adobe_echo_sign_google.user_manager');
        if (!$user = $userManager->findByGoogleUserId($userId)) {
            $userManager->addGoogleUserId($userId, $token);
        } else {
            $userManager->updateToken($token);
        }
    }
}
