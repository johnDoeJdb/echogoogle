<?php

namespace Adobe\EchoSign\GoogleBundle\Security;

class GoogleTestAuthentication
{
    public function auth($username, $password, $redirectUrl)
    {
        function getFormFields($data)
        {
            if (preg_match('/(<form.*?id=.?gaia_loginform.*?<\/form>)/is', $data, $matches)) {
                $inputs = getInputs($matches[1]);

                return $inputs;
            } else {
                die('didnt find login form');
            }
        }

        function getInputs($form)
        {
            $inputs = array();

            $elements = preg_match_all('/(<input[^>]+>)/is', $form, $matches);

            if ($elements > 0) {
                for($i = 0; $i < $elements; $i++) {
                    $el = preg_replace('/\s{2,}/', ' ', $matches[1][$i]);

                    if (preg_match('/name=(?:["\'])?([^"\'\s]*)/i', $el, $name)) {
                        $name  = $name[1];
                        $value = '';

                        if (preg_match('/value=(?:["\'])?([^"\'\s]*)/i', $el, $value)) {
                            $value = $value[1];
                        }

                        $inputs[$name] = $value;
                    }
                }
            }

            return $inputs;
        }


        $USERNAME = $username;
        $PASSWORD = $password;
        $COOKIEFILE = 'cookies.txt';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/6.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $COOKIEFILE);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $COOKIEFILE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);

        curl_setopt($ch, CURLOPT_URL, $redirectUrl);
        $data = curl_exec($ch);

        $formFields = getFormFields($data);

        $formFields['Email']  = $USERNAME;
        $formFields['Passwd'] = $PASSWORD;
        unset($formFields['PersistentCookie']);

        $post_string = '';
        foreach($formFields as $key => $value) {
            $post_string .= $key . '=' . urlencode($value) . '&';
        }

        $post_string = substr($post_string, 0, -1);

        curl_setopt($ch, CURLOPT_URL, 'https://accounts.google.com/ServiceLoginAuth');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);

        $result = curl_exec($ch);

        if (strpos($result, '<title>Redirecting') === false) {
            die("Login failed");
            var_dump($result);
        } else {
            curl_setopt($ch, CURLOPT_URL, 'http://www.google.com/alerts/manage');
            curl_setopt($ch, CURLOPT_POST, 0);
            curl_setopt($ch, CURLOPT_POSTFIELDS, null);

            $result = curl_exec($ch);

            var_dump($result);
        }

        curl_setopt($ch, CURLOPT_URL, $redirectUrl);
        $result = curl_exec($ch);


    }
}