<?php

namespace Adobe\EchoSign\GoogleBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class AdobeEchoSignGoogleExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('google_drive.client_id', $config['google_drive']['client_id']);
        $container->setParameter('google_drive.secret', $config['google_drive']['secret']);
        $container->setParameter('google_drive.folder_agreements_name', $config['google_drive']['folder_agreements_name']);

        $container->setParameter('echo_sign.doc.wsdl', $config['echo_sign']['doc']['wsdl']);
        $container->setParameter('echo_sign.auth.client_id', $config['echo_sign']['auth']['client_id']);
        $container->setParameter('echo_sign.auth.client_secret', $config['echo_sign']['auth']['client_secret']);
        $container->setParameter('echo_sign.auth.redirect_url', $config['echo_sign']['auth']['redirect_url']);
        $container->setParameter('echo_sign.auth.host', $config['echo_sign']['auth']['host']);
    }
}
