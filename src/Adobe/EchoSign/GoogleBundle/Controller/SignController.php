<?php

namespace Adobe\EchoSign\GoogleBundle\Controller;

use Adobe\EchoSign\GoogleBundle\Model\DocumentSenderForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SignController extends Controller
{
    use ControllerTrait;

    /**
     * @Route("/sign", name="sign")
     */
    public function signAction(Request $request)
    {
        $userManager = $this->getUserManager();
        $googleRequestManager = $this->getGoogleRequestManager();
        $googleRequest = $googleRequestManager->fetchRequest();
        $echoSignToken = $userManager->fetchEchoSignToken();
        $googleDriveApi = $this->getGoogleDriveApi();
        $widgetManager = $this->getWidgetManager();
        $fileId = $googleRequest->getFileId();
        $fullFileName = $googleDriveApi->getFullFileName($fileId);

        $docSender = new DocumentSenderForm();
        $form = $this->createForm("sign", $docSender, array(
            "action" => $this->generateUrl("sign")
        ));
        if ("POST" == $request->getMethod())
        {
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $echoSignApi = $this->getEchoSignApi();
                $widgetInfo = $widgetManager->getWidgetInfo($docSender);
                $result = $echoSignApi->createEmbeddedWidget($echoSignToken, $widgetInfo)->embeddedWidgetCreationResult;
                if ($errorMessage = $result->errorMessage)
                {
                    $this->addFlash("error", $errorMessage);
                } else if($result->success) {
                    $this->addFlash("js", $result->javascript);

                    return $this->setP3PHeader()->setQuirksHeader()->render("@AdobeEchoSignGoogle/Sign/interactive.html.twig", array());
                }
                if (property_exists($result, 'errorMessage'))
                {
                    $errorMessage = $result->errorMessage;
                    $this->addFlash("error", $errorMessage);
                }
            }
        }

        return $this->setP3PHeader()->render("AdobeEchoSignGoogleBundle:Sign:index.html.twig", array(
            "form" => $form->createView(),
            "fileName"     => $fullFileName['filename'],
            "fileExt"      => $fullFileName['extension'],
        ));
    }





}
