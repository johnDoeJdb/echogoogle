<?php
namespace Adobe\EchoSign\GoogleBundle\Controller;

use Adobe\EchoSign\GoogleBundle\Entity\Document;
use Adobe\EchoSign\GoogleBundle\Entity\Folder;
use Adobe\EchoSign\GoogleBundle\Model\DriveRequest;
use Proxies\__CG__\Adobe\EchoSign\GoogleBundle\Entity\EchoSignUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CallbackController extends Controller
{
    use ControllerTrait;

    /**
     * @Route("/google/callback/signed_echosign", name="callback_signed_echosign")
     */
    public function signedEchoSignAction()
    {
        $request = $this->get('request');
        $entityManager = $this->getDoctrine()->getManager();
        $documentManager = $this->getDocumentManager();
        $googleDriveApi = $this->getGoogleDriveApi();
        $userId = $request->get('userId');
        $documentKey = $request->get('documentKey');
        $folderId = $request->get('folderId');

        $user = $entityManager->getRepository('AdobeEchoSignGoogleBundle:GoogleUser')->findOneBy(array('userId' => $userId));
        $echoSignUser = $user->getEchoSignUser();
        if (!$folder = $entityManager->getRepository('AdobeEchoSignGoogleBundle:Folder')->findOneBy(array('driveId' => $folderId))) {
            $folder = new Folder();
            $folder->setDriveId($folderId);
        }
        if (!$document = $entityManager->getRepository('AdobeEchoSignGoogleBundle:Document')->findOneBy(array('documentKey' => $documentKey))) {
            $document = new Document();
            $document->setUser($echoSignUser);
            $document->setDocumentKey($documentKey);
            $document->setFolder($folder);
            $entityManager->persist($folder);
            $entityManager->persist($echoSignUser);
        }
        $document->setQueued(true);
        $entityManager->persist($document);
        $entityManager->flush();
        if ($echoSignUser->isValidToken() && $googleDriveApi->isValidToken()) {
            $documentManager->uploadDocuments($user, array($document));
        }

        return Response::create('Got callback from echoSign API');
    }

    /**
     * @Route("/google/token/echosign", name="token_echosign")
     */
    public function echoSignCallbackTokenAction(Request $request)
    {
        $entityManager = $this->get('doctrine.orm.entity_manager');
        $cryptManager = $this->getCryptManager();
        $code = filter_var($request->query->get('code'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
        $state = $request->query->get('state');
        if ($request->query->get('error') && $request->query->get('error_description')) {
            $errorMessage = filter_var($request->query->get('error_description'), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
            throw new BadRequestHttpException($errorMessage);
        }
        $drive = $this->getCryptManager()->decrypt($state);
        if (!$drive instanceof DriveRequest || !$drive->getUserId() || !$drive->getFileId()) {
            throw new BadRequestHttpException("Gotten invalid state from request");
        }
        $echoSignApi = $this->getEchoSignApi();
        $response = $echoSignApi->getAccessOAuthToken($code);
        if ($response && is_object($response)) {
            if (property_exists($response, 'access_token') && property_exists($response, 'refresh_token')) {
                $token = $response->access_token;
                $refreshToken = $response->refresh_token;
                $expirationIn = sprintf('+%d seconds', $response->expires_in);
                $expiration = (new \DateTime())->modify($expirationIn);
                if (!$driveUser = $entityManager->getRepository('AdobeEchoSignGoogleBundle:GoogleUser')->findOneBy(array('userId' => $drive->getUserId()))) {
                    throw new BadRequestHttpException("Drive user not found");
                }
                if (!$echoSignUser = $entityManager->getRepository('AdobeEchoSignGoogleBundle:EchoSignUser')->findOneBy(array('googleUser' => $driveUser))) {
                    $echoSignUser = new EchoSignUser();
                }
                $userInfo = $echoSignApi->getUserInfo($token);

                $echoSignUser->setToken($cryptManager->encrypt($token));
                $echoSignUser->setRefreshToken($cryptManager->encrypt($refreshToken));
                $echoSignUser->setExpireToken($expiration);
                $echoSignUser->setGoogleUser($driveUser);
                $echoSignUser->setEmail($userInfo->getUserInfoResult->data->email);
                $driveUser->setEchoSignUser($echoSignUser);

                $entityManager->persist($echoSignUser);
                $entityManager->persist($driveUser);
                $entityManager->flush();

                return $this->redirectToRoute('sign');
            }
            if (property_exists($response, 'error_description')) {
                $errorMessage = filter_var($response->error_description, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW);
                throw new BadRequestHttpException($errorMessage);
            } else {
                throw new BadRequestHttpException("Token was not issued");
            }
        }
    }
}