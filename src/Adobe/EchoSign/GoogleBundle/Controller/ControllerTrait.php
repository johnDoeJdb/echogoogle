<?php

namespace Adobe\EchoSign\GoogleBundle\Controller;

use Adobe\EchoSign\GoogleBundle\Api\EchoSignApi;
use Adobe\EchoSign\GoogleBundle\Api\GoogleDriveApi;
use Adobe\EchoSign\GoogleBundle\Manager\CryptManager;
use Adobe\EchoSign\GoogleBundle\Manager\DocumentManager;
use Adobe\EchoSign\GoogleBundle\Manager\GoogleRequestManager;
use Adobe\EchoSign\GoogleBundle\Manager\UserManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Adobe\EchoSign\GoogleBundle\Manager\WidgetManager;

trait ControllerTrait
{
    protected $session;
    protected $log;
    protected $protector;
    protected $googleConnector;
    protected $requestManager;
    protected $connector;
    protected $userManager;
    protected $cryptManager;
    protected $widgetManager;

    protected function setQuirksHeader()
    {
        header("X-UA-Compatible: IE=7", TRUE);
        return $this;
    }

    protected function setP3PHeader()
    {
//        header("X-UA-Compatible: IE=edge");
//        header('P3P: CP="CAO PSA OUR IND ONL"');
        return $this;
    }

    protected function getLog()
    {
        if (!isset($this->log))
        {
            $this->log = $this->get("logger");
        }
        return $this->log;
    }

    /**
     * @return EchoSignApi
     */
    protected function getEchoSignApi()
    {
        if (!isset($this->connector))
        {
            $this->connector = $this->get("adobe_echo_sign_google.echosign_api");
        }
        return $this->connector;
    }

    /**
     * @return GoogleRequestManager
     */
    protected function getGoogleRequestManager()
    {
        if (!isset($this->requestManager))
        {
            $this->requestManager = $this->get("adobe_echo_sign_google.request_manager");
        }
        return $this->requestManager;
    }

    /**
     * @return GoogleDriveApi
     */
    protected function getGoogleDriveApi()
    {
        if (!isset($this->googleConnector))
        {
            $this->googleConnector = $this->get("adobe_echo_sign_google.drive_api");
        }
        return $this->googleConnector;
    }

    /**
     * @return DocumentManager
     */
    protected function getDocumentManager()
    {
        if (!isset($this->syncronizer))
        {
            $this->syncronizer = $this->get("adobe_echo_sign_google.document_manager");
        }
        return $this->syncronizer;
    }

    /**
     * @return Session
     */
    protected function getSession()
    {
        if (!isset($this->session))
        {
            $this->session = $this->get("session");
        }
        return $this->session;
    }

    /**
     * @return UserManager
     */
    protected function getUserManager()
    {
        if (!isset($this->userManager))
        {
            $this->userManager = $this->get("adobe_echo_sign_google.user_manager");
        }
        return $this->userManager;
    }

    /**
     * @return CryptManager
     */
    protected function getCryptManager()
    {
        if (!isset($this->cryptManager))
        {
            $this->cryptManager = $this->get("adobe_echo_sign_google.crypt_manager");
        }
        return $this->cryptManager;
    }

    protected function getFlash($name, $default = NULL)
    {
        $flash = $this->getSession()->getFlashBag()->get($name);
        if (is_array($flash))
        {
            $return = array_shift($flash);
            if (empty($return))
            {
                $return = $default;
            }
        }
        else
        {
            $return = $default;
        }
        return $return;
    }

    /**
     * @return WidgetManager
     */
    protected function getWidgetManager()
    {
        if (!isset($this->widgetManager))
        {
            $this->widgetManager = $this->get('adobe_echo_sign_google.widget_manager');
        }
        return $this->widgetManager;
    }
}
