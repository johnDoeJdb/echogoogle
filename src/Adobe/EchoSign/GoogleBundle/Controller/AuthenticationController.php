<?php

namespace Adobe\EchoSign\GoogleBundle\Controller;

use Adobe\EchoSign\GoogleBundle\Entity\EchoSignUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AuthenticationController extends Controller
{
    use ControllerTrait;
    /**
     * @Route("/google/oauth2callback", name="google_oauth2callback")
     */
    public function googleOAuth2CallbackAction()
    {
        $request = $this->get('request_stack')->getCurrentRequest();
        $authenticator = $this->get('adobe_echo_sign_google.google_drive_authentication');
        $authenticator->authenticate($request);

        return $this->setP3PHeader()->render("AdobeEchoSignGoogleBundle:Authentication:modal.html.twig", array(
            'redirect_url' => $this->generateUrl('sign')
        ));
    }
}
