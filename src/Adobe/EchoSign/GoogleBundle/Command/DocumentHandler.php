<?php

namespace Adobe\EchoSign\GoogleBundle\Command;

use Adobe\EchoSign\GoogleBundle\Entity\Document;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class DocumentHandler
{
    CONST PATH_TO_DOWNLOAD = '/tmp/echogoogle/';

    private $container;
    private $userManager;
    /**
     * @var EntityManager
     */
    private $entityManager;
    private $echoSignApi;
    private $googleApi;
    private $googleUser;
    private $echoSignUser;
    private $document;
    private $file;
    private $fileId;
    private $documentName;
    private $currentName;
    private $documentStatus;
    private $latestDocumentKey;
    private $echoSignToken;
    private $googleToken;
    private $cryptManager;

    function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function init($userId)
    {
        $this->userManager = $this->container->get('adobe_echo_sign_google.user_manager');
        $this->entityManager = $this->container->get('doctrine.orm.entity_manager');
        $this->echoSignApi = $this->container->get('adobe_echo_sign_google.echosign_api');
        $this->googleApi = $this->container->get('adobe_echo_sign_google.drive_api');
        $this->cryptManager = $this->container->get('adobe_echo_sign_google.crypt_manager');
        $this->fetchUser($userId);
        $this->echoSignUser = $this->googleUser->getEchoSignUser();
        $this->echoSignToken = $this->cryptManager->decrypt($this->echoSignUser->getToken());
        $this->googleToken = $this->cryptManager->decrypt($this->googleUser->getToken());
        $this->googleApi->setToken($this->googleToken);
    }

    public function handleDocument($documentId)
    {
        $this->fetchDocument($documentId);
        $this->retrieveDocumentInfo();
        $this->generateFileName();

        $this->checkDocument();
        foreach ($this->getTypeDocuments() as $type) {
            $this->generateCurrentName($type);
            $this->download($type);
            $this->upload($type);
        }
        $this->persist();
    }

    private function fetchUser($userId)
    {
        if (!$this->googleUser = $this->userManager->findByGoogleId($userId)) {
            throw new \Exception('Google user does not found or incorrect id');
        };
    }

    private function fetchDocument($id)
    {
        if (!$this->document = $this->entityManager->getRepository('AdobeEchoSignGoogleBundle:Document')->find($id)) {
            throw new \Exception('Document was not found or incorrect id');
        };
    }

    private function retrieveDocumentInfo()
    {
        if (!$info = $this->echoSignApi->getDocumentInfo($this->document->getDocumentKey(), $this->echoSignToken)) {
            throw new \Exception('Document info was not retrieve');
        }
        $this->documentStatus = $info->documentInfo->status;
        $this->documentName = $info->documentInfo->name;
        $this->latestDocumentKey = $info->documentInfo->latestDocumentKey;
    }

    private function generateFileName()
    {
        $name = $this->documentName;
        $userId = $this->googleUser->getUserId();
        if ($repeats = $this->entityManager->getRepository('AdobeEchoSignGoogleBundle:Document')->findNameByPattern($name, $userId)) {
            $numberRepeats = intval($repeats) + 1; $name.= '-'.$numberRepeats;
        }
        $name = str_replace('/', ' ', $name);
        $name = str_replace('\\', ' ', $name);
        $this->documentName = $name;
    }

    private function checkDocument()
    {
        if ($this->document->getLatestDocumentKey() == $this->latestDocumentKey) {
            die;
        }
    }

    private function getTypeDocuments()
    {
        $types = ['auditTrail'];
        if ($this->documentStatus == 'SIGNED') {
            $types[] = 'document';
        }

        return $types;
    }

    private function generateCurrentName($type)
    {
        if ($type == 'document') {
            $this->currentName = $this->documentName.'_signed';
        } else {
            $this->currentName = $this->documentName.'_audit';
        }
    }

    private function download($type)
    {
        $echoSignApi = $this->echoSignApi;
        $documentKey = $this->document->getDocumentKey();
        if ($type == 'document') {
            $response = $echoSignApi->getDocumentUrls($documentKey, $this->echoSignToken);
            if (!$urls = $response->getDocumentUrlsResult->urls) {
                return;
            }
            $url = $urls->DocumentUrl->url;
            $data = @file_get_contents($url);
        } else {
            $auditResult = $echoSignApi->getAuditTrail($documentKey, $this->echoSignToken);
            if (!$data = $auditResult->getAuditTrailResult->auditTrailPdf) {
                return;
            }
        }
        $this->file = $data;
    }

    private function upload()
    {
        $fileId = null;
        try {
            $fileId = $this->googleApi->uploadFile(
                $this->file,
                $this->document->getFolder()->getDriveId(),
                $this->currentName
            );
        } catch (\Exception $e) {
            var_dump($e->getMessage());die;
        }
        $this->fileId = $fileId;
    }

    private function persist()
    {
        $this->document->setGoogleFileId($this->fileId);
        $this->document->setName($this->documentName);
        $this->document->setSigned($this->documentStatus == 'SIGNED');
        $this->document->setQueued(false);
        $this->entityManager->persist($this->document);
        $this->entityManager->persist($this->echoSignUser);
        $this->entityManager->flush();
    }
}