<?php

namespace Adobe\EchoSign\GoogleBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UploaderDocumentCommand extends ContainerAwareCommand
{
    CONST PATH_TO_DOWNLOAD = '/tmp/echogoogle/';

    protected function configure()
    {
        $this
            ->setName('upload:documents')
            ->setDescription('Upload documents by ids')
            ->addArgument('documents', InputArgument::REQUIRED, 'documents ids array in json format')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argumentBase64 = $input->getArgument('documents');
        $data = json_decode(base64_decode($argumentBase64));
        $userId = $data->userId;
        $documentsId = $data->documentsIds;
        $documentHandler = $this->getContainer()->get('adobe_echo_sign_google.upload_handler');
        $documentHandler->init($userId);

        foreach ($documentsId as $documentId) {
            $documentHandler->handleDocument($documentId);
        }
    }
}