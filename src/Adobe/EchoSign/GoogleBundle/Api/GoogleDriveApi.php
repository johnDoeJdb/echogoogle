<?php

namespace Adobe\EchoSign\GoogleBundle\Api;

use Symfony\Component\DependencyInjection\Container;

class GoogleDriveApi
{
    use GoogleDriveApiHelpers;

    private $container;
    private $token;
    private $clientId;
    private $secret;
    private $folderAgreementName;

    function __construct(Container $container)
    {
        $this->container = $container;
        $this->clientId = $container->getParameter('google_drive.client_id');
        $this->secret = $container->getParameter('google_drive.secret');
        $this->folderAgreementName = $container->getParameter('google_drive.folder_agreements_name');
    }

    public function authentication($code, $redirectUrl)
    {
        $client = new \Google_Client();
        $client->setClientId($this->clientId);
        $client->setClientSecret($this->secret);
        $client->setRedirectUri($redirectUrl);
        $client->authenticate($code);

        return $client->getAccessToken();
    }

    public function createAgreementFolder()
    {
        $service = $this->getService();
        $agreementFolder = $this->fetchAgreementFolder();
        if (count($agreementFolder)) {
           $folder = $agreementFolder[0];
        } else {
            $file = new \Google_Service_Drive_DriveFile();
            $file->setMimeType('application/vnd.google-apps.folder');
            $file->setTitle($this->folderAgreementName);
            $folder = $service->files->insert($file, array(
                'mimeType' => 'application/vnd.google-apps.folder',
            ));
        }

        return $folder->getId();
    }

    public function isValidToken()
    {
        $client = $this->getClient();

        return !$client->isAccessTokenExpired();
    }

    public function isExistAgreementFolder()
    {
        $folders = $this->fetchAgreementFolder();

        if ($folders->count()) {
            $folders->current()->getId();
        }

        return $folders->count() !== 0;
    }

    public function fetchFileById($id)
    {
        $service = $this->getService();
        $file = $service->files->get($id);

        return $file;
    }

    public function fetchAllFolders()
    {
        $service = $this->getService();
        $condition = "mimeType = 'application/vnd.google-apps.folder' and trashed = false";
        $folders = $service->files->listFiles(array(
            'q' => $condition
        ));

        return $folders;
    }

    public function fetchParentFolder($fileId)
    {
        $service = $this->getService();
        $file = $this->fetchFileById($fileId);
        $parent = $file->getParents()[0];
        $folder = $service->files->get($parent->getId());

        return $folder;
    }

    public function unTrashFolders($foldersIds)
    {
        $service = $this->getService();

        foreach ($foldersIds as $id) {
            try {
                $service->files->untrash($id);
            } catch (\Exception $e) {}
        }
    }

    public function getFullFileName($fileId)
    {
        $service = $this->getService();
        $file = $service->files->get($fileId);

        $fullName['filename'] = $file->getOriginalFilename();
        $fullName['extension'] = $file->getFileExtension();

        return $fullName;
    }

    public function getFile($fileId)
    {
        $service = $this->getService();

        $file = $service->files->get($fileId);
        $downloadUrl = $file->getDownloadUrl();
        if ($downloadUrl) {
            $request = new \Google_Http_Request($downloadUrl, 'GET', null, null);
            $httpRequest = $service->getClient()->getAuth()->authenticatedRequest($request);
            if ($httpRequest->getResponseHttpCode() == 200) {
                return $httpRequest->getResponseBody();
            } else {
                // An error occurred.
                return null;
            }
        } else {
            // The file doesn't have any content stored on Drive.
            return null;
        }
    }

    public function uploadFile($data, $parentId, $name)
    {
        $service = $this->getService();

        // fetch file if exist
        if ($file = $this->fetchFile($name, $parentId)) {
            $file = $file[0];
            $service->files->delete($file->getId());
        }
        $file = new \Google_Service_Drive_DriveFile();
        if ($parentId != null) {
            $parent = new \Google_Service_Drive_ParentReference();
            $parent->setId($parentId);
            $file->setParents(array($parent));
        }
        $file->setTitle($name);
        $file->setMimeType('application/pdf');
        $file->setFileExtension('pdf');
        $file->setTitle($name);
        $newFile = $service->files->insert($file, array(
            'data' => $data,
            'mimeType' => 'application/pdf',
        ));

        return $newFile->getId();
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    protected function getClient()
    {
        $client = new \Google_Client();
        $client->setClientId($this->clientId);
        $client->setClientSecret($this->secret);
        $client->setAccessToken($this->getToken());

        return $client;
    }

    protected function fetchAgreementFolder()
    {
        $service = $this->getService();
        $folders = [];
        $parameters = array(
            'q' => "
                trashed = false and
                mimeType = 'application/vnd.google-apps.folder' and
                title = '{$this->folderAgreementName}'
            "
        );

        $files = $service->files->listFiles($parameters);
        if ($files->count()) {
            $folders = $files->getItems();
        }

        return $folders;
    }

    protected function fetchFile($name, $parentId)
    {
        $service = $this->getService();
        $parameters = array(
            'q' => "
                trashed = false and
                '{$parentId}' in parents and
                mimeType != 'application/vnd.google-apps.folder' and
                title = '{$name}'
            "
        );

        $files = $service->files->listFiles($parameters);

        return $files->getItems();
    }

    protected function getService()
    {
        return new \Google_Service_Drive($this->getClient());
    }

    private function getToken()
    {
        if (!$this->token) {
            $this->token = $this->container->get('adobe_echo_sign_google.user_manager')->fetchGoogleToken();
        }

        return $this->token;
    }
}
