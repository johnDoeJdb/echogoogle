<?php

namespace Adobe\EchoSign\GoogleBundle\Api;

trait GoogleDriveApiHelpers
{
    protected function getFilesFromAllPages(\Google_Service_Drive_FileList $files)
    {
        $result = [];
        $pageToken = null;

        do {
            try {
                $result = array_merge($result, $files->getItems());
                $pageToken = $files->getNextPageToken();
            } catch (\Exception $e) {
                $pageToken = null;
            }
        } while ($pageToken);

        return $result;
    }
}
