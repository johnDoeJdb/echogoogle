<?php

namespace Adobe\EchoSign\GoogleBundle\Form\Type\General;

use Adobe\EchoSign\GoogleBundle\Manager\TreeFolderManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\DependencyInjection\Container;

class DriveFolderType extends AbstractType
{
    /**
     * @var Container
     */
    private $container;
    private $folders;

    function __construct($container)
    {
        $this->container = $container;
        $this->folders;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $requestManager = $this->container->get('adobe_echo_sign_google.request_manager');
        $googleDriveApi = $this->container->get('adobe_echo_sign_google.drive_api');
        $googleRequest = $requestManager->fetchRequest();
        $parentId = $googleDriveApi->fetchParentFolder($googleRequest->getFileId())->getId();
        $folders = $googleDriveApi->fetchAllFolders();
        $root = $googleDriveApi->fetchFileById('root');
        $root = array(
            'text' => $root['title'],
            'id' => $root['id'],
        );
        $treeFolderManager = new TreeFolderManager($folders);
        $tree = $treeFolderManager->buildTree($root);
        $tree = $treeFolderManager->handleTreeFolder($tree, $parentId);
        $this->folders = json_encode($tree);

        $resolver->setDefaults(array(
            "data" => $parentId,
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['folders'] = $this->folders;
    }


    public function getParent()
    {
        return 'hidden';
    }

    public function getName()
    {
        return 'drive_folder';
    }
}