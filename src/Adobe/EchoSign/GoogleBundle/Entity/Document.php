<?php

namespace Adobe\EchoSign\GoogleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Documents
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Adobe\EchoSign\GoogleBundle\Entity\DocumentRepository")
 */
class Document
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="documentKey", type="string", length=255)
     */
    private $documentKey;

    /**
     * @var string
     *
     * @ORM\Column(name="googleFileId", type="string", length=255, nullable=true)
     */
    private $googleFileId;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="EchoSignUser", inversedBy="documents", cascade={"all"})
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="signed", type="boolean", nullable=true)
     */
    private $signed;

    /**
     * @var string
     *
     * @ORM\Column(name="latest_document_key", type="text", length=255, nullable=true)
     */
    private $latestDocumentKey;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Folder", inversedBy="document", cascade={"all"})
     */
    private $folder;

    /**
     * @var boolean
     *
     * @ORM\Column(name="queued", type="boolean", nullable=true)
     */
    private $queued;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Document
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set documentKey
     *
     * @param string $documentKey
     * @return Document
     */
    public function setDocumentKey($documentKey)
    {
        $this->documentKey = $documentKey;

        return $this;
    }

    /**
     * Get documentKey
     *
     * @return string 
     */
    public function getDocumentKey()
    {
        return $this->documentKey;
    }

    /**
     * Set googleFileId
     *
     * @param string $googleFileId
     * @return Document
     */
    public function setGoogleFileId($googleFileId)
    {
        $this->googleFileId = $googleFileId;

        return $this;
    }

    /**
     * Get googleFileId
     *
     * @return string 
     */
    public function getGoogleFileId()
    {
        return $this->googleFileId;
    }

    /**
     * Set signed
     *
     * @param boolean $signed
     * @return Document
     */
    public function setSigned($signed)
    {
        $this->signed = $signed;

        return $this;
    }

    /**
     * Get signed
     *
     * @return boolean 
     */
    public function getSigned()
    {
        return $this->signed;
    }

    /**
     * Set user
     *
     * @param \Adobe\EchoSign\GoogleBundle\Entity\EchoSignUser $user
     * @return Document
     */
    public function setUser(\Adobe\EchoSign\GoogleBundle\Entity\EchoSignUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Adobe\EchoSign\GoogleBundle\Entity\EchoSignUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set folder
     *
     * @param \Adobe\EchoSign\GoogleBundle\Entity\Folder $folder
     * @return Document
     */
    public function setFolder(\Adobe\EchoSign\GoogleBundle\Entity\Folder $folder = null)
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * Get folder
     *
     * @return \Adobe\EchoSign\GoogleBundle\Entity\Folder 
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @return string
     */
    public function getLatestDocumentKey()
    {
        return $this->latestDocumentKey;
    }

    /**
     * @param string $latestDocumentKey
     */
    public function setLatestDocumentKey($latestDocumentKey)
    {
        $this->latestDocumentKey = $latestDocumentKey;
    }

    /**
     * @return boolean
     */
    public function isQueued()
    {
        return $this->queued;
    }

    /**
     * @param boolean $queued
     */
    public function setQueued($queued)
    {
        $this->queued = $queued;
    }
}
