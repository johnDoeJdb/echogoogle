<?php

namespace Adobe\EchoSign\GoogleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EchoSignUserStorage
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class EchoSignUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_token", type="datetime", nullable=true)
     */
    private $expireToken;

    /**
     * @var string
     *
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="refresh_token", type="string", length=255, nullable=true)
     */
    private $refreshToken;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Document", mappedBy="user", cascade={"all"})
     */
    private $documents;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="GoogleUser")
     */
    private $googleUser;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->documents = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return EchoSignUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return \DateTime
     */
    public function getExpireToken()
    {
        return $this->expireToken;
    }

    /**
     * @param \DateTime $expireToken
     */
    public function setExpireToken($expireToken)
    {
        $this->expireToken = $expireToken;
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return EchoSignUser
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Add documents
     *
     * @param \Adobe\EchoSign\GoogleBundle\Entity\Document $documents
     * @return EchoSignUser
     */
    public function addDocument(\Adobe\EchoSign\GoogleBundle\Entity\Document $documents)
    {
        $this->documents[] = $documents;

        return $this;
    }

    /**
     * Remove documents
     *
     * @param \Adobe\EchoSign\GoogleBundle\Entity\Document $documents
     */
    public function removeDocument(\Adobe\EchoSign\GoogleBundle\Entity\Document $documents)
    {
        $this->documents->removeElement($documents);
    }

    /**
     * Get documents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set googleUser
     *
     * @param \Adobe\EchoSign\GoogleBundle\Entity\GoogleUser $googleUser
     * @return EchoSignUser
     */
    public function setGoogleUser(\Adobe\EchoSign\GoogleBundle\Entity\GoogleUser $googleUser = null)
    {
        $this->googleUser = $googleUser;

        return $this;
    }

    /**
     * Get googleUser
     *
     * @return \Adobe\EchoSign\GoogleBundle\Entity\GoogleUser 
     */
    public function getGoogleUser()
    {
        return $this->googleUser;
    }

    public function isValidToken()
    {
        $now = new \DateTime();
        $tokenExpires = $this->getExpireToken();
        $isValid = $tokenExpires ? ($tokenExpires > $now) : false;

        return $isValid;
    }
}
