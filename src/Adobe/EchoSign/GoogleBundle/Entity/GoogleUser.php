<?php

namespace Adobe\EchoSign\GoogleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BoxUserStorage
 *
 * @ORM\Table()
 * @ORM\Entity()
 */
class GoogleUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="userId", type="string", length=255)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="EchoSignUser")
     */
    private $echoSignUser;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="text", nullable=true)
     */
    private $token;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return GoogleUser
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return string 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return GoogleUser
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set echoSignUser
     *
     * @param \Adobe\EchoSign\GoogleBundle\Entity\EchoSignUser $echoSignUser
     * @return GoogleUser
     */
    public function setEchoSignUser(\Adobe\EchoSign\GoogleBundle\Entity\EchoSignUser $echoSignUser = null)
    {
        $this->echoSignUser = $echoSignUser;

        return $this;
    }

    /**
     * Get echoSignUser
     *
     * @return \Adobe\EchoSign\GoogleBundle\Entity\EchoSignUser 
     */
    public function getEchoSignUser()
    {
        return $this->echoSignUser;
    }
}
