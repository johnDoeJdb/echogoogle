<?php

namespace Adobe\EchoSign\GoogleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Documents
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Adobe\EchoSign\GoogleBundle\Entity\FolderRepository")
 */
class Folder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="driveId", type="string", nullable=true)
     */
    private $driveId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="Document", mappedBy="folder", cascade={"all"})
     */
    private $document;
    /**
     * Constructor
     */
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->document = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set driveId
     *
     * @param integer $driveId
     * @return Folder
     */
    public function setDriveId($driveId)
    {
        $this->driveId = $driveId;

        return $this;
    }

    /**
     * Get driveId
     *
     * @return integer 
     */
    public function getDriveId()
    {
        return $this->driveId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Folder
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add document
     *
     * @param \Adobe\EchoSign\GoogleBundle\Entity\Document $document
     * @return Folder
     */
    public function addDocument(\Adobe\EchoSign\GoogleBundle\Entity\Document $document)
    {
        $this->document[] = $document;

        return $this;
    }

    /**
     * Remove document
     *
     * @param \Adobe\EchoSign\GoogleBundle\Entity\Document $document
     */
    public function removeDocument(\Adobe\EchoSign\GoogleBundle\Entity\Document $document)
    {
        $this->document->removeElement($document);
    }

    /**
     * Get document
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDocument()
    {
        return $this->document;
    }
}
