<?php
namespace Adobe\EchoSign\GoogleBundle\Manager;

use Adobe\EchoSign\GoogleBundle\Entity\GoogleUser;
use Symfony\Component\DependencyInjection\Container;

class RestoreManager
{
    /**
     * @var Container
     */
    private $container;

    function __construct($container)
    {
        $this->container = $container;
    }

    function restoreFoldersIfTrashed()
    {
        $userManager = $this->container->get('adobe_echo_sign_google.user_manager');
        $googleApi = $this->container->get('adobe_echo_sign_google.drive_api');
        $entityManager = $this->container->get('doctrine.orm.entity_manager');

        $foldersIds = $entityManager->getRepository('AdobeEchoSignGoogleBundle:Folder')->getFoldersByUserId($userManager->fetchCurrentEchoSignUser()->getId());
        $googleApi->unTrashFolders($foldersIds);
    }
}
