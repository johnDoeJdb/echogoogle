<?php

namespace Adobe\EchoSign\GoogleBundle\Manager;

use Adobe\EchoSign\GoogleBundle\Model\DocumentSenderForm;
use Adobe\EchoSign\GoogleBundle\Model\WidgetInfo;
use Symfony\Component\DependencyInjection\Container;

/**
 * A skeleton for the user data, used in the registration form.
 */
class WidgetManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getWidgetInfo(DocumentSenderForm $docSender)
    {
        $userManager = $this->container->get('adobe_echo_sign_google.user_manager');
        $googleDriveApi = $this->container->get('adobe_echo_sign_google.drive_api');
        $requestManager = $this->container->get("adobe_echo_sign_google.request_manager");
        $googleRequest = $requestManager->fetchRequest();
        $widgetInfo = new WidgetInfo();
        $googleUser = $userManager->fetchCurrentUser();
        $urlParameters = array(
            'userId' => $googleUser->getUserId(),
            'folderId' => $docSender->getFolder(),
        );
        $serverIp = $_SERVER['SERVER_ADDR'];

        $widgetInfo->setCallbackUrl('https://'.$serverIp.'/google/callback/signed_echosign?'.http_build_query($urlParameters));
        $widgetInfo->setEmails($docSender->getEmails());
        $widgetInfo->setRoles($docSender->getRoles());
        $widgetInfo->setCcs(array_values($docSender->getCcs()));
        $widgetInfo->setName($docSender->getName());
        $widgetInfo->setFileName($googleDriveApi->getFullFileName($googleRequest->getFileId())['filename']);
        $widgetInfo->setFile($googleDriveApi->getFile($googleRequest->getFileId()));
        $widgetInfo->setLocale($docSender->getLocale());
        $widgetInfo->setSignatureFlow($docSender->getSignatureFlow());
        $widgetInfo->setProtection($docSender->getVerifyWithPassword());
        $widgetInfo->setPassword($docSender->getPassword());
        $widgetInfo->setProtectOpen(!!($docSender->getProtectOpen()));

        $widgetInfo = $this->validate($widgetInfo);

        return $widgetInfo;
    }

    private function validate(WidgetInfo $widgetInfo)
    {
        $flashBag = $this->container->get("session")->getFlashBag();
        if (!$widgetInfo->getFile()) {
            $flashBag->add('error', 'Application can not download selected file from Google Drive');

            return null;
        }
        if (!$widgetInfo->getEmails()) {
            $flashBag->add('error', 'Please provide at least one recipient');

            return null;
        }
        if (!$widgetInfo->getCallbackUrl()) {
            $flashBag->add('error', 'Callback url is emtpy. Please contact with administrator');

            return null;
        }
    }
}