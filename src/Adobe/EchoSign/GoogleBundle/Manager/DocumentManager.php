<?php

namespace Adobe\EchoSign\GoogleBundle\Manager;

use Adobe\EchoSign\GoogleBundle\Command\UploaderDocumentCommand;
use Adobe\EchoSign\GoogleBundle\Entity\GoogleUser;
use Symfony\Component\DependencyInjection\Container;

/**
 * A skeleton for the user data, used in the registration form.
 */
class DocumentManager
{
    private $container;

    function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function downloadQueuedDocuments(GoogleUser $user)
    {
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $documents = $entityManager->getRepository('AdobeEchoSignGoogleBundle:Document')->fetchQueuedDocuments($user->getId());
        if (count($documents)) {
            $this->uploadDocuments($user, $documents);
        }
    }

    public function restoreFolder(GoogleUser $user)
    {
        $documents = $this->fetchUserDocumentsFromDatabase($user);
        $this->uploadDocuments($user, $documents, $type = 'restore');
    }

    public function uploadDocuments(GoogleUser $user, array $documents, $type = null)
    {
        $ids = array();
        foreach ($documents as $document) {
            $ids[] = $document->getId();
        }

        $this->backgroundDownloader($user->getId(), $ids, $type);
    }

    private function backgroundDownloader($userId, $documentsIds, $type = null)
    {
        $parameters = array(
            'userId' => $userId,
            'documentsIds' => $documentsIds,
            'type' => $type,
        );
        $parameters = base64_encode(json_encode($parameters));
        $command = 'php '.WEB_DIRECTORY.'/../app/console upload:documents '.$parameters.' &> /dev/null &';

        exec($command);
    }

    private function fetchUserDocumentsFromDatabase(GoogleUser $user)
    {
        $entityManager = $this->container->get('doctrine.orm.entity_manager');
        $documents = $entityManager->getRepository('AdobeEchoSignGoogleBundle:Document')->fetchDocumetsByUserId($user->getId());

        return $documents;
    }
}