<?php

namespace Adobe\EchoSign\GoogleBundle\Manager;

use Adobe\EchoSign\GoogleBundle\Entity\EchoSignUser;
use Adobe\EchoSign\GoogleBundle\Entity\GoogleUser;

class UserManager
{
    private $entityManager;
    private $session;
    private $cryptManager;

    function __construct($entityManager, $session, $cryptManager)
    {
        $this->cryptManager = $cryptManager;
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    /**
     * @return GoogleUser
     */
    public function fetchCurrentUser()
    {
        $googleRequest = $this->session->get('google');

        return $this->findByGoogleUserId($googleRequest->getUserId());
    }

    /**
     * @return EchoSignUser
     */
    public function fetchCurrentEchoSignUser()
    {
        return $echoSignUser = $this->fetchCurrentUser()->getEchoSignUser();
    }

    public function fetchGoogleToken()
    {
        return $this->cryptManager->decrypt($this->fetchCurrentUser()->getToken());
    }

    public function fetchEchoSignToken()
    {
        return $this->cryptManager->decrypt($this->fetchCurrentEchoSignUser()->getToken());
    }

    public function fetchEchoSignRefreshToken()
    {
        return $this->cryptManager->decrypt($this->fetchCurrentEchoSignUser()->getRefreshToken());
    }

    /**
     * @return GoogleUser
     */
    public function findByGoogleId($id)
    {
        $user = $this->entityManager->getRepository('AdobeEchoSignGoogleBundle:GoogleUser')->find($id);

        return $user;
    }

    /**
     * @return GoogleUser
     */
    public function findByGoogleUserId($userId)
    {
        $user = $this->entityManager->getRepository('AdobeEchoSignGoogleBundle:GoogleUser')->findOneBy(array(
            'userId' => $userId,
        ));

        return $user;
    }

    public function addGoogleUserId($userId, $token)
    {
        $user = new GoogleUser();
        $user->setUserId($userId);
        $user->setToken($this->cryptManager->encrypt($token));

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function updateToken($token)
    {
        $token = $this->cryptManager->encrypt($token);
        $user = $this->fetchCurrentUser();
        $user->setToken($token);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function updateEchoSignToken($token, $expiration = null)
    {
        $token = $this->cryptManager->encrypt($token);
        $user = $this->fetchCurrentUser()->getEchoSignUser();
        $user->setToken($token);
        if (!$expiration) {
            $expiration = ((new \DateTime())->modify('+1 hour'));
        }
        $user->setExpireToken($expiration);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
