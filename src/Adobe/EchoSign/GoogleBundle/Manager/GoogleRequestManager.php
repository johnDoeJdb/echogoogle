<?php

namespace Adobe\EchoSign\GoogleBundle\Manager;

use Adobe\EchoSign\GoogleBundle\Model\DriveRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class GoogleRequestManager
{
    private $session;

    function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function getAuthCode(Request $request)
    {
        $code = filter_var($request->query->get('code'), FILTER_SANITIZE_STRING);

        return $code;
    }

    public function buildDriveRequest(Request $request)
    {
        $driveRequest = new DriveRequest();

        if (!$state = $request->query->get('state')) {
            return null;
        }

        if (is_object($googleRequest = json_decode($state))) {
            $driveRequest->setUserId(filter_var($googleRequest->userId, FILTER_SANITIZE_NUMBER_INT));
            $driveRequest->setFileId(filter_var($googleRequest->ids[0], FILTER_SANITIZE_STRING));
            $driveRequest->setAction(filter_var($googleRequest->action, FILTER_SANITIZE_STRING));
        };

        $this->session->set('google', $driveRequest);
    }


    /**
     * @return DriveRequest
     */
    public function fetchRequest()
    {
        return $this->session->get('google');
    }
}
