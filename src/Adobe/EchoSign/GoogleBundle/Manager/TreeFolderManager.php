<?php

namespace Adobe\EchoSign\GoogleBundle\Manager;

class TreeFolderManager
{
    public $files;

    function __construct($files)
    {
        $this->files = $files;
    }

    public function buildTree($root)
    {
        $buildTree = function($root) use (&$buildTree)  {
            foreach ($this->files['items'] as $file) {
                if ($root['id'] === $file['parents'][0]['id']) {
                    $node = ['id' => $file['id'], 'text' => $file['title']];
                    $root['children'][] = $buildTree($node);
                }
            }

            return $root;
        };
        $root = $buildTree($root);

        return $root;
    }


    public function handleTreeFolder($folders, $parent)
    {
        $setSelection = function (&$folders) use ($parent, &$setSelection) {
            foreach ($folders as &$folder) {
                if (array_key_exists('children', $folder)) {
                    $setSelection($folder['children']);
                }
                if ($folder['id'] == $parent) {
                    $folder['state'] = array(
                        'selected' => true,
                    );
                }
            }
        };
        $folders = array($folders);
        $setSelection($folders);

        return $folders;
    }
}
